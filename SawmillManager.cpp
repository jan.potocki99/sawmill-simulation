#include "SawmillManager.hpp"


SawmillManager::SawmillManager(int id, Resources* resources, std::vector<SawmillWorker*> workers):
    id(id),
    resources(resources),
    workers(workers),
    thread(&SawmillManager::cycle,this)
    {}


void SawmillManager::cycle(){

    while (running)
    {
        createOrder();
    }
    
}

void SawmillManager::createOrder(){

    for(SawmillWorker *s : this->workers ){
        if(s->getHasOrder() == false){
            if(s->getBoardType() == "SHORT" && s->getId() != lastShortCallId){
                s->setHasOrder(true);
                lastShortCallId = s->getId();
            }
            if(s->getBoardType() == "LONG" && s->getId() != lastLongCallId){
                s->setHasOrder(true);
                lastLongCallId = s->getId();

            }
        }
    }
    
}

void SawmillManager::setRunning(bool running){
    this->running = running;
}

void SawmillManager::join(){
    this->thread.join();
}