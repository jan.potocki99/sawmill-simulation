#include "Lumberjack.hpp"

Lumberjack::Lumberjack(int id, std::vector<Tree *> trees):
    id(id),
    trees(trees),
    treeId(-1),
    thread(&Lumberjack::cycle, this)
    {}


Tree* Lumberjack::findTree(){

    int id = rand() % 10;

    if(this->trees[id]->state == TreeState::READY && !this->trees[id]->getOwner()){
            return this->trees[id];

    }

    return nullptr;
    
}

void Lumberjack::cycle(){

    Tree *tree;

    while (this->running)
    {
        tree = findTree();

        if(tree != NULL){
            this->state = LumberjackState::CUTTING;
            this->treeId = tree->getId();

            bool cutTree = true;

            while(cutTree && running){
                tree->mutex.lock();
                cutTree = tree->cut();
                tree->mutex.unlock();
            }

            this->treeId = -1;
            
        }

        this->state = LumberjackState::RESTING;
        usleep(100000);
    }

    this->done = true;

}

void Lumberjack::setRunning(bool running){
  this->running = running;
}

void Lumberjack::join(){
  this->thread.join();
}

int Lumberjack::getId(){
    return this->id;
}

int Lumberjack::getTreeId(){
    return this->treeId;
}

std::string Lumberjack::getState()
{
  switch (state)
  {
  case LumberjackState::WAITING:
    return "WAITING";
  case LumberjackState::RESTING:
    return "RESTING";
  case LumberjackState::CUTTING:
    return "CUTTING";
  default:
    return "UNKNOWN";
  }
}
