#pragma once
#include "Resources.hpp"
#include <mutex>
#include "BoardDriverState.hpp"
#include <string.h>
#include <thread>
#include <atomic>
#include <unistd.h>

class BoardDriver
{
private:
    int id;
    Resources* resources;
    std::thread thread;
    bool running = true;
    int driveTime = 5000000;
    int longBoards = 5;
    int shortBoards = 5;

public:
    std::atomic<BoardDriverState> state {BoardDriverState::WAITING};
    BoardDriver(int id, Resources * resources);
    void cycle();
    void join();
    void setRunning(bool running);

    int getId();
    std::string getState();

};

