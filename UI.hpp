#pragma once
#include <thread>
#include <ncurses.h>
#include "Manager.hpp"
#include "Resources.hpp"
#include "Tree.hpp"
#include "Lumberjack.hpp"
#include "WoodDriver.hpp"
#include "SawmillWorker.hpp"
#include "SawmillManager.hpp"
#include "BoardDriver.hpp"
#include <unistd.h>
#include <string.h>
#include <vector>

class UI
{
private:
    std::thread refreshThread;
    Manager* manager;
    Resources* resources;
    SawmillManager* sawmillManager;
    std::vector<Tree *> trees;
    std::vector<Lumberjack*> lumberjacks;
    std::vector<WoodDriver*> woodDrivers;
    std::vector<SawmillWorker*>sawmillWorkers;
    std::vector<BoardDriver*>boardDrivers;
    bool running;
public:
    UI(Manager* manager);
    ~UI();
    void init();
    void update();
    void finish();
};
