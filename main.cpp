#include <stdio.h>
#include "UI.hpp"
#include "Manager.hpp"

int main(int argc, char const *argv[])
{
    srand(time(NULL));
    Manager* manager = new Manager();
    UI *ui = new UI(manager);

    ui->init();

    return 0;
}
