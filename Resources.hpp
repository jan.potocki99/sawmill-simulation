#pragma once
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "BoardType.hpp"

class Resources
{
private:
    std::atomic<int> wood{0};
    std::atomic<int> deliveredWood{0};
    std::atomic<int> shortBoards{0};
    std::atomic<int> longBoards{0};

     
    std::atomic<int> totalBoardsInWarehouse{0};
    std::atomic<int> shortBoardsInWarehouse{0};
    std::atomic<int> longBoardsInWarehouse{0};

    bool running = true;
    std::condition_variable cv;
    std::mutex mutex;

public:
    Resources();
    int getWood();
    int getDeliveredWood();
    int getShortBoards();
    int getLongBoards();
    int getShortBoardsInWarehouse();
    int getLongBoardsInWarehouse();
    int getTotalBoardsInWarehouse();

    bool getRunning();
    void setRunning(bool running);

    void addWood(int wood);
    void requestWood(int requestedWood);

    void addDeliveredWood(int wood);
    void requestDeliveredWood(int requestedWood);

    void addShortBoard(int amount);
    void addLongBoard(int amount);
    void addBoardsToWarehouse(int shortAmount, int longAmount);


    void requestShortBoard(int amount);
    void requestLongBoard(int amount);

    void addBoardsToWarehouse(int short, int long);

    
};

