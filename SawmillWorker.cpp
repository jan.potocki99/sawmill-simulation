#include "SawmillWorker.hpp"

SawmillWorker::SawmillWorker(int id,Resources* resources, BoardType boardType):
    id(id),
    resources(resources),
    boardType(boardType),
    thread(&SawmillWorker::cycle, this)
    {}

void SawmillWorker::cycle(){

    while (this->running)
    { 
        work();
    }
    
}



void SawmillWorker::join(){
    this->thread.join();
}

void SawmillWorker::setRunning(bool running){
    this->running = running;
}

int SawmillWorker::getId(){
    return this->id;
}

std::string SawmillWorker::getState()
{
  switch (this->state)
  {
  case SawmillWorkerState::REST:
    return "REST";
  case SawmillWorkerState::WORK:
    return "WORK";
  default:
    return "UNKNOWN";
  }
}

std::string SawmillWorker::getBoardType()
{
  switch (this->boardType)
  {
  case BoardType::SHORT:
    return "SHORT";
  case BoardType::LONG:
    return "LONG";
  default:
    return "UNKNOWN";
  }
}

SawmillWorkerState SawmillWorker::getWorkerType(){
  return this->state;
}

bool SawmillWorker::getHasOrder(){
  return this->hasOrder;
}

void SawmillWorker::setHasOrder(bool order){
  std::lock_guard<std::mutex> lock(mutex);
  this->hasOrder = order;
  cv.notify_one();
}

void SawmillWorker::work(){
    std::unique_lock<std::mutex> ul(mutex);
    if(running == false) return;
    cv.wait(ul, [&]{
      if(this->hasOrder){
        return true;
      }
      return false;
    });

  if(this->boardType == BoardType::SHORT){
    this->resources->requestDeliveredWood(this->shortBoardMaterial);
    this->state = SawmillWorkerState::WORK;
    usleep((rand() % workTime) + workTime );
    this->resources->addShortBoard(5);
    this->state = SawmillWorkerState::REST;
    usleep((rand() % workTime) + workTime );
    this->hasOrder = false;
  }

  if(this->boardType == BoardType::LONG){
    this->resources->requestDeliveredWood(this->longBoardMaterial);
    this->state = SawmillWorkerState::WORK;
    usleep((rand() % workTime) + workTime );
    this->resources->addLongBoard(5);
    this->state == SawmillWorkerState::REST;
    usleep((rand() % workTime) + workTime );
    this->hasOrder = false;        
  }

}

int SawmillWorker::getPreparedOrders(){
  return this->preparedOrder;
}