#pragma once

#include <mutex>
#include <thread>
#include <atomic>
#include "TreeState.hpp"
#include <unistd.h>
#include <time.h>
#include "Resources.hpp"

class Tree
{
private:
    std::thread thread;
    bool running = true;
    bool done = false;
    int id;
    std::atomic<int> growth{0};
    std::atomic<bool> hasOwner{false};
    int growthTime = 500000;
    int cuttingTime = 500000;
    Resources * resources;


public:

    std::mutex mutex;
    std::atomic<TreeState> state{TreeState::GROWING};

    Tree(int id, Resources *resources);
    ~Tree();
    void treeCycle();
    void setRunning(bool running);
    void finish();
    void join();
    bool cut();
    void setOwner(bool hasOwner);

    int getId();
    int getGrowth();
    std::string getState();
    bool getOwner();

    
};

