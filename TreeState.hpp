#pragma once

enum class TreeState {GROWING,  READY, CUTTING};