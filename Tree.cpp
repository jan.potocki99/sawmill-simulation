#include "Tree.hpp"

Tree::Tree(int id, Resources *resources):
    id(id),
    resources(resources),
    thread(&Tree::treeCycle, this)
    {}


void Tree::treeCycle(){
    while (this->running)
    {
        if(this->state == TreeState::GROWING){
            this->growth += rand() % 10;

            if(this->growth >= 100){
                state = TreeState::READY;
            }
            usleep((rand() % 1000000) + this->growthTime);
        }
    }

    this->done = true;
    
}

bool Tree::cut(){
    if(this->state == TreeState::GROWING) return false;
    this->state = TreeState::CUTTING;
    if(this->running){
        usleep((rand() % cuttingTime) + cuttingTime + 100000);
        this->growth = 0;
        this->resources->addWood(5);
        this->state = TreeState::GROWING;
        return false;
    }

    return true;
}

void Tree::setRunning(bool running){
    this->running = running;
}

 std::string Tree::getState() {
    switch(state) {
        case TreeState::READY:
        return "READY";
        case TreeState::GROWING:
        return "GROWING";
        case TreeState::CUTTING:
        return "CUTTING";
        default:
        return "UNKNOWN";
  }
 }

 void Tree::setOwner(bool hasOwner){
     this->mutex.lock();
     this->hasOwner = hasOwner;
     this->mutex.unlock();
 }

 void Tree::join(){
     this->thread.join();
 }

 int Tree::getId(){
     return id;
 }

 bool Tree::getOwner(){
     return hasOwner;
 }

 int Tree::getGrowth(){
     return growth;
 }