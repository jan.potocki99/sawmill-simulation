#include "Manager.hpp"


Manager::Manager(){

    resources = new Resources();
    trees = std::vector<Tree *>();
    lumberjacks = std::vector<Lumberjack*>();
    woodDrivers = std::vector<WoodDriver*>();
    sawmillWorkers = std::vector<SawmillWorker*>();
    boardDrivers = std::vector<BoardDriver*>();

    

    for(int i=0;i<TREES;i++){
        trees.push_back(new Tree(i,resources));
    }

    for(int i=0;i<LUMBERJACK_NUMBER;i++){
        lumberjacks.push_back(new Lumberjack(i,trees));
    }

    for(int i=0; i< WOOD_DRIVER; i++){
        woodDrivers.push_back(new WoodDriver(i,resources));
    }

    for(int i=0; i< BOARD_DRIVER; i++){
        boardDrivers.push_back(new BoardDriver(i,resources));
    }

    sawmillWorkers.push_back(new SawmillWorker(0,resources, BoardType::SHORT));
    sawmillWorkers.push_back(new SawmillWorker(1,resources, BoardType::SHORT));
    sawmillWorkers.push_back(new SawmillWorker(2,resources, BoardType::LONG));
    sawmillWorkers.push_back(new SawmillWorker(3,resources, BoardType::LONG));

    sawmillManager = new SawmillManager(0,resources,sawmillWorkers);


}

void Manager::exit(){

    resources->setRunning(false);

   for(int i=0;i<TREES;i++){
        trees[i]->setRunning(false);
    }

    for(int i=0;i<LUMBERJACK_NUMBER;i++){
        lumberjacks[i]->setRunning(false);
    }

    for(int i=0; i<WOOD_DRIVER; i++){
        woodDrivers[i]->setRunning(false);
    }

    for(int i=0; i<SAWMILL_WORKER; i++){
        sawmillWorkers[i]->setRunning(false);
    }

     for(int i=0; i<BOARD_DRIVER; i++){
        boardDrivers[i]->setRunning(false);
    }

    sawmillManager->setRunning(false);

    //////////////////////////
    // JOIN THREADS
    /////////////////////////

    for(int i=0;i<TREES;i++){
        trees[i]->join();
    }

    for(int i=0;i<LUMBERJACK_NUMBER;i++){
        lumberjacks[i]->join();
    }

    for(int i=0; i<WOOD_DRIVER; i++){
        woodDrivers[i]->join();
    }

    for(int i=0; i<SAWMILL_WORKER; i++){
        sawmillWorkers[i]->join();
    }

    for(int i=0; i<BOARD_DRIVER; i++){
        boardDrivers[i]->join();
    }
    
    sawmillManager->join();

    
}

Resources* Manager::getResources(){
    return this->resources;
}

SawmillManager* Manager::getSawmillManager(){
    return this->sawmillManager;
}

std::vector<Tree *> Manager::getTrees(){
    return trees;
}

std::vector<Lumberjack*> Manager::getLumberjacks(){
    return lumberjacks;
}

std::vector<WoodDriver*> Manager::getWoodDrivers(){
    return woodDrivers;
}

std::vector<SawmillWorker*> Manager::getSawmillWorkers(){
    return sawmillWorkers;
}

std::vector<BoardDriver*> Manager::getBoardDrivers(){
    return boardDrivers;
}