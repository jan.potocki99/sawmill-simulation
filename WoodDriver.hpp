#pragma once
#include "Resources.hpp"
#include <mutex>
#include "WoodDriverState.hpp"
#include <string.h>
#include <thread>
#include <atomic>
#include <unistd.h>


class WoodDriver
{
private:
    int id;
    bool running = true;
    bool done = false;
    int loadCapacity = 30;
    int driveTime = 5000000;
    std::atomic<int> woodInTransport {0};
    std::thread thread;
    Resources* resources;


public:
    std::mutex mutex;
    std::atomic<WoodDriverState> state {WoodDriverState::WAITING};

    WoodDriver(int id, Resources* resources);
    void cycle();
    void finish();
    void join();
    void setRunning(bool running);

    int getId();
    std::string getState();

   
};


