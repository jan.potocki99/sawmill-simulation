#pragma once
#include "Resources.hpp"
#include "BoardType.hpp"
#include <mutex>
#include <string.h>
#include <thread>
#include <atomic>
#include <unistd.h>
#include <condition_variable>
#include <mutex>
#include "SawmillWorker.hpp"
#include <vector>


class SawmillManager
{
private:
    int id;
    std::thread thread;
    bool running = true;
    std::atomic<bool> orderDone {false};
    std::condition_variable cv;
    Resources* resources;
    std::vector<SawmillWorker*> workers;
    int lastShortCallId = -1;
    int lastLongCallId = -1;

public:
    std::mutex mutex;
    SawmillManager(int id, Resources* resources, std::vector<SawmillWorker*> workers);
    void setRunning(bool running);
    void createOrder();
    void join();
    void cycle();
};

