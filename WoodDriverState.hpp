#pragma once

enum class WoodDriverState {WAITING,  TO_SAWMILL, TO_FOREST};