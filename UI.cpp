#include "UI.hpp"
#define RESOURCES 120
#define TREES 0
#define LUMBERJACKS 30
#define WOOD_DRIVER 60
#define SAWMILL_WORKER 80

UI::UI(Manager* manager): manager(manager),running(true){

    resources = manager->getResources();
    
    trees = manager->getTrees();
    lumberjacks = manager->getLumberjacks();
    woodDrivers = manager->getWoodDrivers();
    sawmillWorkers = manager->getSawmillWorkers();
    boardDrivers = manager->getBoardDrivers();
    sawmillManager = manager->getSawmillManager();
}

UI::~UI(){
    endwin();
}

void UI::init(){

    initscr();
    start_color();
    init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_BLACK);

    keypad(stdscr, TRUE);
    refreshThread = std::thread(&UI::update, this);

    
    while (getch() != 27)
    {

    }

    this->running = false;
    manager->exit();
    refreshThread.join();
    endwin();
    
}

void UI::update(){

    while(running){

        erase();
        ////////////////////////////////
        //     TREES
        ///////////////////////////////
        attron(COLOR_PAIR(1));
        mvprintw(0, 17, "TREES");
        attroff(COLOR_PAIR(1));
        attron(COLOR_PAIR(2));
        mvprintw(2, 2, "ID");
        mvprintw(2, 6, "STATE");
        mvprintw(2, 16, "PROGRESS");
        attroff(COLOR_PAIR(2));


        for(Tree* t: this->trees){
            mvprintw(t->getId()+4, TREES + 2, std::to_string(t->getId()).c_str());
            mvprintw(t->getId()+4, TREES + 6, t->getState().c_str());
            mvprintw(t->getId()+4, TREES + 16, std::to_string(t->getGrowth()).c_str());

        }



        ///////////////////////////////
        //      RESOURCES
        ///////////////////////////////
        attron(COLOR_PAIR(1));
        mvprintw(0,RESOURCES + 7,"RESOURCES");
        attroff(COLOR_PAIR(1));
        attron(COLOR_PAIR(2));
        mvprintw(2,RESOURCES, "NAME");
        mvprintw(2,RESOURCES + 20,"AMOUNT");
        attroff(COLOR_PAIR(2));

        mvprintw(4,RESOURCES,"WOOD");
        mvprintw(4,RESOURCES + 20, std::to_string(resources->getWood()).c_str());
        mvprintw(5,RESOURCES, "DELIVERED WOOD");
        mvprintw(5,RESOURCES + 20, std::to_string(resources->getDeliveredWood()).c_str());
        attron(COLOR_PAIR(2));
        mvprintw(7,RESOURCES, "SAWMILL BOARDS");
        attroff(COLOR_PAIR(2));

        mvprintw(8,RESOURCES, "SHORT");
        mvprintw(8,RESOURCES + 20, std::to_string(resources->getShortBoards()).c_str());
        mvprintw(9,RESOURCES, "LONG");
        mvprintw(9,RESOURCES + 20, std::to_string(resources->getLongBoards()).c_str());

        attron(COLOR_PAIR(2));
        mvprintw(11,RESOURCES, "WAREHOUSE BOARDS");
        attroff(COLOR_PAIR(2));

        mvprintw(12,RESOURCES, "SHORT");
        mvprintw(12,RESOURCES + 20, std::to_string(resources->getShortBoardsInWarehouse()).c_str());
         mvprintw(13,RESOURCES, "LONG");
        mvprintw(13,RESOURCES + 20, std::to_string(resources->getLongBoardsInWarehouse()).c_str());
        mvprintw(14,RESOURCES, "TOTAL");
        mvprintw(14,RESOURCES + 20, std::to_string(resources->getTotalBoardsInWarehouse()).c_str());


        ////////////////////////////////
        //     LUMBERJACKS
        ///////////////////////////////
        attron(COLOR_PAIR(1));
        mvprintw(0, LUMBERJACKS + 12, "LUMBERJACKS");
        attroff(COLOR_PAIR(1));

        attron(COLOR_PAIR(2));
        mvprintw(2,LUMBERJACKS +  2, "ID");
        mvprintw(2, LUMBERJACKS + 8, "STATE");
        mvprintw(2, LUMBERJACKS + 16, "TREE_ID");
        attroff(COLOR_PAIR(2));


        for(Lumberjack* l: this->lumberjacks){
            mvprintw(l->getId()+4, LUMBERJACKS + 2, std::to_string(l->getId()).c_str());
            mvprintw(l->getId()+4, LUMBERJACKS + 8, l->getState().c_str());
            mvprintw(l->getId()+4, LUMBERJACKS + 16, std::to_string(l->getTreeId()).c_str());


        }

        ////////////////////////////////
        //     DRIVERS
        ///////////////////////////////
        attron(COLOR_PAIR(1));
        mvprintw(0, WOOD_DRIVER + 8, "DRIVERS");
        attroff(COLOR_PAIR(1));
        attron(COLOR_PAIR(2));
        mvprintw(2,WOOD_DRIVER +  2, "WOOD DRIVERS");
        attroff(COLOR_PAIR(2));
        mvprintw(3,WOOD_DRIVER +  2, "ID");
        mvprintw(3, WOOD_DRIVER + 8, "STATE");

        for(WoodDriver* w: this->woodDrivers){
            mvprintw(w->getId()+4, WOOD_DRIVER + 2, std::to_string(w->getId()).c_str());
            mvprintw(w->getId()+4, WOOD_DRIVER + 8, w->getState().c_str());


        }

        attron(COLOR_PAIR(2));
        mvprintw(7,WOOD_DRIVER +  2, "BOARD DRIVERS");
        attroff(COLOR_PAIR(2));
        mvprintw(8,WOOD_DRIVER +  2, "ID");
        mvprintw(8, WOOD_DRIVER + 8, "STATE");

        for(BoardDriver* b: this->boardDrivers){
            mvprintw(b->getId()+9, WOOD_DRIVER + 2, std::to_string(b->getId()).c_str());
            mvprintw(b->getId()+9, WOOD_DRIVER + 8, b->getState().c_str());


        }


        ////////////////////////////////
        //     SAWMILL WORKERS
        ///////////////////////////////
        attron(COLOR_PAIR(1));
        mvprintw(0, SAWMILL_WORKER + 12, "SAWMILL WORKERS");
        attroff(COLOR_PAIR(1));
        attron(COLOR_PAIR(2));
        mvprintw(2,SAWMILL_WORKER +  2, "ID");
        mvprintw(2, SAWMILL_WORKER + 6, "STATE");
        mvprintw(2, SAWMILL_WORKER + 12, "BOARD TYPE");
        attroff(COLOR_PAIR(2));

        for(SawmillWorker* s: this->sawmillWorkers){
            mvprintw(s->getId()+4, SAWMILL_WORKER + 2, std::to_string(s->getId()).c_str());
            mvprintw(s->getId()+4, SAWMILL_WORKER + 6, s->getState().c_str());
            mvprintw(s->getId()+4, SAWMILL_WORKER + 14, s->getBoardType().c_str());


        }




        refresh();
        usleep(10000);
    }
}