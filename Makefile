all: main

main: main.cpp Resources.cpp UI.cpp Manager.cpp Tree.cpp Lumberjack.cpp
	g++ -std=c++17 -pthread -Wall *.cpp -o main -lncurses && ./main

clear:
	rm main
