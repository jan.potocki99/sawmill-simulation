#pragma once
#include "Resources.hpp"
#include "Tree.hpp"
#include <vector>
#include "Lumberjack.hpp"
#include "WoodDriver.hpp"
#include "SawmillWorker.hpp"
#include "BoardDriver.hpp"
#include "SawmillManager.hpp"

#define TREES 10
#define LUMBERJACK_NUMBER  5
#define WOOD_DRIVER 2
#define SAWMILL_WORKER 4
#define BOARD_DRIVER 2


class Manager
{
private:
    Resources* resources;
    SawmillManager* sawmillManager;
    std::vector<Tree*> trees;
    std::vector<Lumberjack*> lumberjacks;
    std::vector<WoodDriver*> woodDrivers;
    std::vector<SawmillWorker*> sawmillWorkers;
    std::vector<BoardDriver*> boardDrivers;

public:
    Manager();
    ~Manager();
    void exit();
    Resources* getResources();
    SawmillManager* getSawmillManager();
    std::vector<Tree *> getTrees();
    std::vector<Lumberjack*> getLumberjacks();
    std::vector<WoodDriver*> getWoodDrivers();
    std::vector<SawmillWorker*> getSawmillWorkers();
    std::vector<BoardDriver*> getBoardDrivers();
};


