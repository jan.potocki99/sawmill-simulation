#include "BoardDriver.hpp"

BoardDriver::BoardDriver(int id, Resources* resources):
    id(id),
    resources(resources),
    thread(&BoardDriver::cycle, this)
    {}


void BoardDriver::join(){
    this->thread.join();
}

void BoardDriver::setRunning(bool running){
    this->running = running;
}

void BoardDriver::cycle(){

    while (running)
    {
        if(this->state == BoardDriverState::WAITING){
            resources->requestShortBoard(shortBoards);
            resources->requestLongBoard(longBoards);

            this->state = BoardDriverState::TO_WAREHOUSE;
            usleep((rand() % driveTime) + driveTime);
            resources->addBoardsToWarehouse(shortBoards,longBoards);
            this->state = BoardDriverState::TO_SAWMILL;
            usleep((rand() % driveTime) + driveTime);
            this->state = BoardDriverState::WAITING;


        }
        
    }
    
}

std::string BoardDriver::getState() {
    switch(state) {
        case BoardDriverState::WAITING:
        return "WAITING";
        case BoardDriverState::TO_WAREHOUSE:
        return "TO WAREHOUSE";
        case BoardDriverState::TO_SAWMILL:
        return "TO SAWMILL";
        default:
        return "UNKNOWN";
  }
}

int BoardDriver::getId(){
    return this->id;
}