#pragma once

enum class BoardDriverState {WAITING,  TO_WAREHOUSE, TO_SAWMILL};