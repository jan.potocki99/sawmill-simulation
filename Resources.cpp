#include "Resources.hpp"

Resources::Resources(){}

bool Resources::getRunning(){
    return running;
}

void Resources::setRunning(bool running){
    this->running=running;
}

void Resources::addWood(int wood){
    std::lock_guard<std::mutex> lock(mutex);
    this->wood+=wood;
    cv.notify_all();
}

void Resources::requestWood(int request){
    std::unique_lock<std::mutex> ul(mutex);
    if(running == false) return;
    
    cv.wait(ul, [&]{
        if(this->wood >= request){
            return true;
        }

        return false;
    });

    this->wood -= request;
    
    
    
}

void Resources::addDeliveredWood(int wood){
    std::lock_guard<std::mutex> lock(mutex);
    this->deliveredWood+=wood;
    cv.notify_all();
}

void Resources::requestDeliveredWood(int request){
    std::unique_lock<std::mutex> ul(mutex);
    if(running == false) return;
    cv.wait(ul, [&]{
        if(this->deliveredWood >= request){
            return true;
        }

        return false;
    });

    this->deliveredWood -= request;
}

void Resources::addShortBoard(int amount){
    std::lock_guard<std::mutex> lock(mutex);
    this->shortBoards+=amount;
    cv.notify_all();
}

void Resources::addLongBoard(int amount){
    std::lock_guard<std::mutex> lock(mutex);
    this->longBoards+=amount;
    cv.notify_all();
}

void Resources::requestShortBoard(int amount){
    std::unique_lock<std::mutex> ul(mutex);
    if(running == false) return;
    cv.wait(ul, [&]{
        if(this->shortBoards >= amount){
            return true;
        }

        return false;
    });

    this->shortBoards -= amount;

}

void Resources::requestLongBoard(int amount){
    std::unique_lock<std::mutex> ul(mutex);
    if(running == false) return;
    cv.wait(ul, [&]{
        if(this->longBoards >= amount){
            return true;
        }

        return false;
    });

    this->longBoards -= amount;

}

void Resources::addBoardsToWarehouse(int shortAmount, int longAmount){
    std::lock_guard<std::mutex> lock(mutex);
    this->longBoardsInWarehouse+=longAmount;
    this->shortBoardsInWarehouse+=shortAmount;

    cv.notify_all();
}


int Resources::getWood(){
    return wood;
}

int Resources::getShortBoards(){
    return shortBoards;
}

int Resources::getLongBoards(){
    return longBoards;
}

int Resources::getShortBoardsInWarehouse(){
    return shortBoardsInWarehouse;
}

int Resources::getLongBoardsInWarehouse(){
    return longBoardsInWarehouse;
}

int Resources::getTotalBoardsInWarehouse(){
    return totalBoardsInWarehouse;
}

int Resources::getDeliveredWood(){
    return deliveredWood;
}