#pragma once

#include "thread"
#include "mutex"
#include "Resources.hpp"
#include "BoardType.hpp"
#include "SawmillWorkerState.hpp"
#include <unistd.h>
#include <condition_variable>

class SawmillWorker
{
private:
   int id;
   bool running = true;
   std::thread thread;
   Resources* resources;
   BoardType boardType;
   int shortBoardMaterial = 3;
   int longBoardMaterial = 5;
   std::condition_variable cv;
   std::atomic<bool> hasOrder{false};
   int preparedOrder = 0;
   int workTime = 10000000;

public:

    std::mutex mutex;
    std::atomic<SawmillWorkerState> state {SawmillWorkerState::REST};

    SawmillWorker(int id, Resources* resources, BoardType boardType);
    
    void cycle();
    void setRunning(bool running);
    void join();

    int getId();
    std::string getState();
    std::string getBoardType();
    SawmillWorkerState getWorkerType();
    bool getHasOrder();
    void setHasOrder(bool order);
    void work();
    int getPreparedOrders();

};

