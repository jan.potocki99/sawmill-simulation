#include "WoodDriver.hpp"

WoodDriver::WoodDriver(int id, Resources * resources):
    id(id),
    resources(resources),
    thread(&WoodDriver::cycle,this)
    {}


void WoodDriver::cycle(){

    while (this->running)
    {
        if(this->state == WoodDriverState::WAITING){
            this->resources->requestWood(loadCapacity);
            this->state = WoodDriverState::TO_SAWMILL;
            usleep((rand() % driveTime) + driveTime);
            this->resources->addDeliveredWood(loadCapacity);
            this->state = WoodDriverState::TO_FOREST;
            usleep((rand() % driveTime) + driveTime);
            this->state = WoodDriverState::WAITING;
            
        }
    }
    
}

void WoodDriver::join(){
    this->thread.join();
}

void WoodDriver::setRunning(bool running){
    this->running = running;
}

std::string WoodDriver::getState() {
    switch(state) {
        case WoodDriverState::WAITING:
        return "WAITING";
        case WoodDriverState::TO_FOREST:
        return "TO FOREST";
        case WoodDriverState::TO_SAWMILL:
        return "TO SAWMILL";
        default:
        return "UNKNOWN";
  }
}

int WoodDriver::getId(){
    return this->id;
}