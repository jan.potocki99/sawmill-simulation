#pragma once
#include "Tree.hpp"
#include "LumberjackState.hpp"
#include <thread>
#include <mutex>
#include <atomic>
#include <vector>

#define LUMBERJACK_NUMBER  5

class Lumberjack
{
private:
    std::thread thread;
    std::mutex mutex;
    int id;
    int treeId;
    bool running = true;
    bool done = false;
    std::atomic<LumberjackState> state {LumberjackState::WAITING};
    std::vector <Tree *> trees;
    int cuttingTime = 5000000;
    
public:
    Lumberjack(int id, std::vector<Tree *> trees);
    void setRunning(bool running);
    void join();
    void finish();
    void cycle();
    Tree* findTree();
    
    bool getRunning();
    int getId();
    std::string getState();
    int getTreeId();

};

